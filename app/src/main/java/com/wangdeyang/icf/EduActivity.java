package com.wangdeyang.icf;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//code reference for this module is as follows
//    https://codinginflow.com/tutorials/android/text-to-speech
public class EduActivity extends AppCompatActivity {

    List<String> lesson = new ArrayList<String>();
    private Button preButton;
    private int count = -1;

    // Fields for speaker
    private TextToSpeech mTTS_US;
    private TextToSpeech mTTS_UK;
    private EditText mEditText;
    private SeekBar mSeekBarPitch;
    private SeekBar mSeekBarSpeed;
    private Button mButtonSpeak_US;
    private Button mButtonSpeak_UK;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edu);

        // configureSubmitButton();
        /*lesson.add("What is the purpose for this research study? (1/2)\n " + "\n" +
                "1.This research study is for approximately 40 patients at 4 study sites who have non-Hodgkin’s lymphoma and have received treatment that is no longer working. \n" +
                "2.The purpose of this study is to determine if STUDY DRUG CX-012 is safe and can reduce or prevent the growth of cancer cells \n");
*/
        lesson.add("What is the purpose for this research study?(2/2)\n"+"\n" +
                "3.STUDY DRUG CX-012 is not approved by the Food and Drug Administration (FDA).\n" +
                "4.This consent form will tell the possible risks, inconveniences, and discomforts, and other general principles that people who take part in the study should know.\n");
        lesson.add("What will happen if I enroll?\n" +"\n" +
                "1.If you agree to be in this study, you will take STUDY DRUG CX-012 for up to 52 weeks.\n" +
                "2.You will take the study drug by mouth with water at the same time each day, for 21 days every 28 days.\n" +
                "3.You will be expected to bring your empty study drug bottles and any unused study drug back to the clinic at each visit every 4 weeks.\n");

        lesson.add("What tests and procedures will I have done?(1/3)\n"+"\n" +
                "If you decide to participate in this study, you will have the following procedures every 4 weeks:\n"+
                "1.Medical history\n" +
                "2.Review of all medications you are taking prior to starting the study\n"+
                "3.Blood pressure and pulse\n");
        lesson.add("What tests and procedures will I have done?(2/3)\n"+"\n" +
                "4.Routine blood tests. Up to 2 tablespoons of blood may be taken at each study visit.\n" +
                "5.Evaluation of your ability to perform your daily activities.\n"+
                "6.Physical examination.\n"+
                "7.Pregnancy test\n");
        lesson.add("What tests and procedures will I have done?(3/3)\n" +"\n" +
                "8.Electrocardiogram (ECG) - recording of your heart’s electrical activity.\n"+
                "9.CT scans (computed tomography) or MRI (magnetic resonance imaging)\n");

        lesson.add("Optional procedures may be performed if your cancer appears to have gone away:(1/3)\n"+"\n" +
                "1.Bone marrow aspiration and biopsy (if required) - removal of a small amount of bone marrow fluid and bone marrow tissue through a needle inserted into the bone.\n");
        lesson.add("Optional procedures may be performed if your cancer appears to have gone away:(2/3) \n "+"\n" +
                "2.Tissue (i.e. lymph node) biopsy (if required) - removal of a small amount of tissue through a needle inserted in the tissue or an open biopsy (removal of the tissue by a surgeon).\n");
        lesson.add("Optional procedures may be performed if your cancer appears to have gone away:(3/3)\n" +"\n" +
                "3.Lumbar puncture/spinal tap (if required) - A procedure in which a needle is put into the lower part of the spinal column to collect fluid. Explanations of the risks of these optional procedures can be found in the informed consent\n");

        lesson.add("What if I am pregnant or plan on becoming pregnant?\n"+"\n" +
                "Women of child bearing potential will be required to have a negative pregnancy test 7 days prior to tarting on STUDY DRUG CX-012, then every 4 weeks. You will be required to use birth control if you are in this study. Only if you have had a hysterectomy or no menstrual periods for at least 24 months in a row, will you not be required to have these pregnancy tests and use birth control.\n");

        lesson.add("What are the Risks?(1/3)\n"+"\n" +
                "Your condition may not get better or may become worse while you are in this study.\n"+
                "There are risks involved in taking STUDY DRUG CX-012. There maybe side effects, some of which are not yet known. Problems which have not been seen before may be serious or deadly.\n");
        lesson.add("What are the Risks?(2/3)\n"+"\n" +
                "The most common side effects of STUDY DRUG CX-012 include:(1/3)\n"+"\n" +
                "Low number of infection\n" +
                "fighting white blood cells\n" +
                "Fatigue/ tiredness\n" +
                "Low blood clotting factors\n" +
                "Low red blood cell count\n" +
                "Diarrhea\n" +
                "Low white blood cell count\n");
        lesson.add("What are the Risks?(2/3)\n"+"\n" +
                "The most common side effects of STUDY DRUG CX-012 include:(2/3)\n" +"\n" +
                "Constipation\n" +
                "Rash\n" +
                "Cough\n" +
                "Nausea\n" +
                "Abdominal pain\n" +
                "Joint pain\n" +
                "Difficulty sleeping\n");
        lesson.add("What are the Risks?(2/3)\n"+"\n" +
                "The most common side effects of STUDY DRUG CX-012 include:(3/3)\n" +"\n" +
                "High liver enzymes\n" +
                "High blood sugar\n" +
                "Muscle cramps\n" +
                "Peripheral edema or\n" +
                "swelling\n" +
                "Sore throat\n" +
                "Itching\n" +
                "Fever\n");
        lesson.add("What are the Risks?(3/3)\n"+"\n" +
                "If any other doctor prescribes medication for you for another condition, you must inform the study staff. This is important because the interaction of some medications may cause serious side effects.\n");

        lesson.add("What are the Benefits?(1/2)\n"+"\n" +
                "You may get better or have symptoms improve by taking the study drug. There are no promises that you will get good results by being in this study.\n");
        lesson.add("What are the Benefits?(2/2)\n"+"\n" +
                "The study sponsor will provide the investigational drug, STUDY DRUG CX-012, at no cost. You and/or your insurance company are responsible for the costs of any other treatment\n" +
                "Future patients with the same disease may benefit from the results\n" +
                "of this study.\n"+
                "You will receive no payment for taking part in this study.\n");

        lesson.add("Do I have other treatment options?(1/3)\n"+"\n" +
                "You do not have to be in this study to receive treatment for your Non-Hodgkin’s lymphoma.\n" +
                "If you decide not to enter this study, there are other treatments available, which may include:\n" +
                "1.Supportive care or drugs similar to those used in this study which are currently approved, or\n" +
                "2.Drugs that are the subject of other clinical studies investigating the treatment of Non-Hodgkin’s lymphoma.\n");
        lesson.add("Do I have other treatment options?(2/3)\n"+"\n" +
                "There are different opinions as to whether the alternative treatments would benefit you; therefore, it is important that you discuss the benefits and risks of all treatment options with your Study Doctor before signing this consent form.\n");
        lesson.add("Do I have other treatment options?(3/3)\n"+"\n" +
                "You may also choose to have no further treatment for your Non-Hodgkin’s lymphoma.\n" +
                "You may talk with your doctor about these and other options before you agree to enter the study, and about other options that may become available during the study.\n");

        lesson.add("How is my personal information protected?(1/2)\n"+"\n" +
                "If you choose to be in this study, the Study Doctor will obtain personal information about you. This may include information that may identify you and describe your health including medical and research records. Federal regulations give you the right to know who will be able to get the information and why they may be able to get it.\n");
        lesson.add("How is my personal information protected?(2/2)\n"+"\n" +
                "The Study Doctor must get your authorization (permission) to use or give out any health information that might identify you. A list of people that the study doctor may share your information with is in the informed consent.\n");

        lesson.add("What if I don’t want to be in the research study?(1/2)\n"+"\n" +
                "Your participation is voluntary and you may choose not to participate or stop participating in the study at any time. There is no penalty or loss of benefits that you deserve.\n" +
                "If you choose not to participate or to withdraw from study therapy, you will still be offered all available care that suits your needs and medical condition. \n");
        lesson.add("What if I don’t want to be in the research study?(2/2)\n"+"\n" +
                "Your participation in this study may stopped by the doctor if your disease progresses, you experience unacceptable side effects, you do not follow the study instructions, or the study is discontinued by the sponsor.\n" +
                "If you leave the study, please contact Dr. Olsen or one of the study associates who will tell you what you should do before leaving. You may be asked to return to the clinic for follow-up care, if necessary.\n");

        lesson.add("Who can I ask questions about the research study?(1/3)\n"+"\n" +
                "The Study Doctor and/or his/her staff are available to answer any questions you might have. If you have any questions about being in this study, the study procedures or study treatment, contact:\n"+
                "Dr. Mary Olsen at (919) 555-5555\n");
        lesson.add("Who can I ask questions about the research study?(2/3)\n"+"\n" +
                "If you decide to participate, you will be told about any new information developed during this study that might change your decision to be in this study.\n" +
                "If you want further information about your rights as a subject participating in a research study, you may contact:\n"+
                "Ryan Hamm at (919) 555-1234\n");
        lesson.add("Who can I ask questions about the research study?(3/3)\n"+"\n" +
                "If an injury occurs to you as a direct result of being given the investigational drug (STUDY DRUG CX- 012), you should contact:\n"+
                "Benjamin Jones at (919) 555-7777\n");

        lesson.add("Do not sign this consent form unless you have had a chance to ask questions and have received satisfactory answers to all of your questions.\n");




        configureNextButton();
        configurePreButton();

        mButtonSpeak_US = findViewById(R.id.button_speak_US);
        mButtonSpeak_UK = findViewById(R.id.button_speak_UK);


        mTTS_US = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTTS_US.setLanguage(Locale.US);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Language not supported");
                    } else {
                        mButtonSpeak_US.setEnabled(true);
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });


        mTTS_UK = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTTS_UK.setLanguage(Locale.UK);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Language not supported");
                    } else {
                        mButtonSpeak_UK.setEnabled(true);
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });

        mEditText = findViewById(R.id.Lesson);
        mSeekBarPitch = findViewById(R.id.seek_bar_pitch);
        mSeekBarSpeed = findViewById(R.id.seek_bar_speed);

        mEditText.setEnabled(false);

        mButtonSpeak_US.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak_US();
            }
        });

        mButtonSpeak_UK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak_UK();
            }
        });


    }

    private void configureNextButton() {
        Button nextButton = findViewById(R.id.NextLesson);
        final Button takeButton = findViewById(R.id.GoToQuiz);

        takeButton.setVisibility(View.GONE);
        takeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EduActivity.this, Q1.class));
            }
        });


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count< lesson.size()-1){
                    count++;
                }
                EditText editText = (EditText)findViewById(R.id.Lesson);
                editText.setText(lesson.get(count), TextView.BufferType.EDITABLE);

                if (count == lesson.size()-1){
                    takeButton.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void configurePreButton() {
        preButton = findViewById(R.id.preButton);
        preButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 0){
                    count--;
                }
//                else{
//                    Toast.makeText(EduActivity.this,
//                            "this is the first question", Toast.LENGTH_LONG).show();
//                }
                EditText editText = (EditText)findViewById(R.id.Lesson);
                editText.setText(lesson.get(count), TextView.BufferType.EDITABLE);
            }

        });

    }



    private void speak_US() {
        String text = mEditText.getText().toString();
        float pitch = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1) pitch = 0.1f;
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1) speed = 0.1f;

        mTTS_US.setPitch(pitch);
        mTTS_US.setSpeechRate(speed);

        mTTS_US.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void speak_UK() {
        String text = mEditText.getText().toString();
        float pitch = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1) pitch = 0.1f;
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1) speed = 0.1f;

        mTTS_UK.setPitch(pitch);
        mTTS_UK.setSpeechRate(speed);

        mTTS_UK.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    @Override
    protected void onDestroy() {
        if (mTTS_US != null) {
            mTTS_US.stop();
            mTTS_US.shutdown();
        }

        if (mTTS_UK != null) {
            mTTS_UK.stop();
            mTTS_UK.shutdown();
        }

        super.onDestroy();
    }
}
